import AbstractProbe from '../models/AbstractProbe.mjs'
import Axios from 'axios'
import Telegraf from 'telegraf'

export default class TooGoodToGo extends AbstractProbe {
    static services = [
        {
            name: "Check Favorites",
            description: "Notify when a shop in favorite list have something to sell",
            func: "check_fav",
            interval: 60000
        },
        {
            name: "Auto book subscribed items",
            description: "Try to automatically book items tracked by the bot",
            func: "autoBook",
        }
    ]

    constructor(args) {
        super('TooGoodToGo', TooGoodToGo.services)

        if(args.length < 3) {
            throw new TypeError('Not enough argument to instantiate class')
        }
        const [user_id, user_token, chat_id] = args
        // Common constants
        this.USER_ID = user_id.trim()
        this.USER_TOKEN = user_token.trim()
        this.CHAT_ID = chat_id
        this.BEARER = Buffer.from(`ACCESS:${this.USER_ID}:${this.USER_TOKEN}`).toString('base64')

        this.bot = new Telegraf('1104990802:AAHDvsXOHYvlxpLZ_Dl1DnMnR_TypEj1iUQ')
    }

    check_fav() {
        // Specialized constants
        const API_URL = 'https://apptoogoodtogo.com/api/item/v4/'
        const PAYLOAD = {
            "user_id": this.USER_ID,
            "origin": {
                "latitude": 0,
                "longitude": 0
            },
            "radius": 0,
            "favorites_only": true
        }

        const config = {
            headers: {
                'Authorization': `Bearer ${this.BEARER}`,
                'Content-Type': 'application/json'
            }
        }

        Axios.post(
            API_URL,
            PAYLOAD,
            config
        ).then(res => {
            let msg = `Actual state:`
            res.data.items.forEach(item => {
                const itemName = (!!item.item.name) ? `(${item.item.name})` : ''
                msg += `\n${item.store.store_name} ${item.store.branch} ${itemName}: ${item.items_available} item(s) available.`
            })
            this.bot.telegram.sendMessage(this.CHAT_ID, msg)
        }).catch(err => {
            console.log(err)
        })
    }

    autoBook() {

    }
}