export default class AbstractProbe {
    static services = [
        {
            name: "Test",
            func: "test",
            interval: 300000, // 300000 = 5 min
            timeoutObject: null
        }
    ]

    constructor(name, services) {
        if (this.constructor === AbstractProbe) {
            throw new TypeError('Abstract class "AbstractProbe" cannot be instantiated directly')
        }
        this.name = name || 'AbstractProbe'
        this.services = (!!services)
            ? services
            : AbstractProbe.services
    }

    test() {
        console.log("Test !")
    }

    run() {
        console.log(`Module ${this.name} launched.`)
        this.services.forEach((service, i) => {
            console.log(`Service ${service.name} installed, will launch all ${service.interval} ms.`)
            this.services[i].timeoutObject  = setInterval(this[service.func].bind(this), service.interval)
        })
    }

    stop() {
        console.log(`Module ${this.name} removed.`)
        this.services.forEach((service) => {
            console.log(`Service ${service.name} stopped.`)
            clearInterval(service.timeoutObject)
        })
    }
}