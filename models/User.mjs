import mongoose from '../config/database.mjs'
import bcrypt from 'bcryptjs'

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
    set: pwd => bcrypt.hashSync(pwd, 10)
  },
  roles: {
    type: [String],
    lowercase: true,
    default: ['user']
  },
  created_at: {
      type: Date,
      default: Date.now,
      required: true
  },
  updated_at: {
      type: Date,
      default: Date.now,
      required: true
  },
  deleted_at: {
      type: Date,
      default: null,
      required: false
    }
});

export default mongoose.model('User', userSchema)