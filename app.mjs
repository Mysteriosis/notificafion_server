import Koa from 'koa'
import Pug from 'koa-pug'
// import Sass from 'koa-sass'
import Logger from 'koa-logger'
import Sentry from '@sentry/node'
import BodyParser from 'koa-bodyparser'
import MomentJS from 'moment'
import Comedy from 'comedy'
import Telegraf from 'telegraf'
import LocalSession from 'telegraf-session-local'

import fs from 'fs'

Sentry.init({ dsn: 'https://b02abf3de5fe4a72bd0e632f86c783a9@sentry.io/3772875' })

// NOTIFICATIONS MANAGER
const modules = []
const actorSystem = Comedy()

function importClass(filename, index) {
   import('./probes/' + filename).then(f => {
      if(index === null || index === -1) {
         const module = {
            filename: filename,
            name: f.default.name,
            class: f.default
         }
         modules.push(module)
      }
   }).catch(e => {
      if(index > -1) {
         // TODO: Kill all instances of this class
         modules.splice(index, 1)
      }
   })
}

// Module loader (Probes)
fs.readdir('./probes', function (err, files) {
   if (err) {
      throw new Error('Unable to scan directory: ' + err)
   }
   files.forEach(function (filename) {
      importClass(filename, null)
   })
})

const bot = new Telegraf('1104990802:AAHDvsXOHYvlxpLZ_Dl1DnMnR_TypEj1iUQ')
bot.use((new LocalSession({ database: 'services/chats.json' })).middleware())

// Liste des services disponibles
bot.command('services', (ctx) => {
   let response = 'Services availables:'
   modules.forEach(module => {
      response += `\n*${module.name}*`;
      module.class.services.forEach((service, i) => {
         response += `\n${i+1}. ${service.name}\n   (${service.description})`
      })
   })
   ctx.replyWithMarkdown(response)
})

// Souscrire à un service
bot.command('subscribe', ctx => {
   const args = ctx.message.text.split(' ')
   const calledModule = modules.find(m => m.name === args[1])
   if(calledModule === undefined) {
      ctx.replyWithMarkdown(`_Unknown module_`)
      return
   }
   if(args[2] > 0) {
      args.push(ctx.message.chat.id)
      const moduleInstance = new calledModule.class(args.slice(3))
      const calledService = moduleInstance.services[args[2]-1]
      moduleInstance[calledService.func]()
      ctx.replyWithMarkdown(`Successfully subscribed to the "${moduleInstance.name}" service, of the *${calledModule.name}* module.`)
   }
   else {
      ctx.replyWithMarkdown(`_Wrong service ID_`)
   }
})

bot.startPolling()

// Module watcher (hot reload)
fs.watch('./probes', (eventType, filename) => {
   const index = modules.findIndex(cl => cl.filename === filename)
   if(eventType === 'rename' && filename.split('.').pop() === 'mjs') {
      importClass(filename, index)
   }
})


// WEB SERVER
import config from './config/config.mjs'
import router from './config/routes.mjs'

const app = new Koa();

// Init middlewares
app.use(Logger());
app.use(BodyParser());
app.use(router.routes());

const pug = new Pug({
   viewPath: './views',
   basedir: './views',
   noCache: config.app.env === 'development',
   locals: {
      moment: MomentJS
   },
   app: app
});

// Error handler in production
app.use(async (ctx, next) => {
   if(config.app.env === 'production') {
      try {
         await next();
         if ((ctx.status || 404) === 404)
            ctx.throw(404)
      } catch (err) {
         ctx.status = err.status || 500;
         ctx.body = err.message;
         ctx.app.emit('error', err, ctx);
      }
   }
});

// Start server
// app.listen(config.app.port)