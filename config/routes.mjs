import Router from 'koa-router'

// Controllers
import HomeController from '../controllers/homeController.mjs'
import AdminController from '../controllers/adminController.mjs'

const router = new Router()
const homeController = new HomeController()
const adminController = new AdminController()

/** 
 * Home page : Display home page
 * @return ./views/home.pug
 */
router.get("/", async (ctx) => {
    await homeController.index(ctx)
});

/** 
 * Admin page : Display admin page
 * @return ./views/admin/index.pug
 */
router.get("/admin", async (ctx) => {
    await adminController.index(ctx)
});

export default router;