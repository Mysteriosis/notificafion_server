import Controller from './controller.mjs'

class homeController extends Controller {
    async index(ctx) {
        ctx.render('home.pug', {
            title: "Home page"
        })
    }
}

export default homeController