export default class Controller {
    constructor(model) {
        this.model = (model !== undefined) ? model : null;
    }

    index(req) {
        return (!!this.model) ? this.model.find({}) : []
    }

    create(req) {

    }

    save(req) {

    }

    delete(req) {

    }
}
