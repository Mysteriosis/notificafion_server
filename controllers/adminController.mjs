import Controller from './controller.mjs'

class adminController extends Controller {
    async index(ctx) {
        ctx.render('admin/index.pug', {
            title: "Config page"
        })
    }
}

export default adminController