import Jwt from 'koa-jwt'
import config from '../config/config.mjs'

export default Jwt({
  secret: config.jwt.secret
})