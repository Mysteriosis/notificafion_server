import assert from 'assert'

import User from '../../models/User.mjs'

// DATA
const users = [
    { "_id" : "5c65ed0b3580a510ccc7e005", "roles" : [ "admin", "user" ], "email" : "pacurty@gmail.com", "username" : "Iosis555", "password" : "sokolov" }
]

// SAVE
const saves = [
    User.insertMany(users, { rawResult: true }, function(err, r) {
        assert.equal(null, err)
        assert.equal(users.length, r.insertedCount)
        User.db.close()
    }),
]

Promise.all(saves, function() {
    process.exit()
})